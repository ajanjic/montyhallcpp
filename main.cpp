#include "easyRandom.h"
#include <iostream>
#include <fstream>

int main()
{
	long long int res[3] = { 0 }; //0 - no change, 1 - change, 2 - no. attempts
	lameutil::EasyRandom rng;

	std::fstream file("out.txt", std::ios::out | std::ios::trunc);

	for (long long int i = 0; i < 1000000; i++)
	{
		res[2]++;
		int winningDoor = rng.getInt(0, 3);
		int attempt = rng.getInt(0, 3);

		if (winningDoor == attempt)
			res[0]++;
		else
			res[1]++;


		if (i == 100 || i == 1000 || i == 20000 || i == 100000)
		{
			file << "Attempts: " << i
				<< "\nWins after not switching: " << res[0]
				<< "\nWins after switching: " << res[1]
				<< "\nSwitching win rate: " << (double)res[1] / res[2] << std::endl << std::endl;
		}
	}

	file.close();
}